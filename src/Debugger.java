import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Optional;
import java.util.function.Function;

/*
 * @author Thiago dos Santos Martins
 * @version 0.5
 * @since   02/09/2020 (MM/DD/YYYY)
 *
 * Last update: 02/11/2020
 * 
 */

enum DebugTextColor {
	BLUE {
		public String color() {
			return "01";
		}
	},
	GREEN {
		public String color() {
			return "02";
		}
	},
	AQUA {
		public String color() {
			return "03";
		}
	},
	RED {
		public String color() {
			return "04";
		}
	},
	PURPLE {
		public String color() {
			return "05";
		}
	},
	YELLOW {
		public String color() {
			return "06";
		}
	},
	WHITE {
		public String color() {
			return "07";
		}
	},
	GRAY {
		public String color() {
			return "08";
		}
	},
	LBLUE {
		public String color() {
			return "09";
		}
	},
	LGREEN {
		public String color() {
			return "0A";
		}
	},
	LAQUA {
		public String color() {
			return "0B";
		}
	},
	LRED {
		public String color() {
			return "0C";
		}
	},
	LPURPLE {
		public String color() {
			return "0D";
		}
	},
	LYELLOW {
		public String color() {
			return "0E";
		}
	},
	BWHITE {
		public String color() {
			return "0F";
		}
	};
	
	public abstract String color();
}

public class Debugger 
{
	public static boolean ON = true;
	
	private PrintStream printStream;
	private File        fileBat;
	private Process     process;
	private Runtime     runtime;
	
	public Debugger() throws IOException {
		fileBat = new File("debugger.bat");
		runtime = Runtime.getRuntime();
		
		printStream = new PrintStream(fileBat);
		printStream.println("@echo off");
		printStream.println(":start");
		printStream.println("MODE 120,30");
		printStream.println("TITLE Debug Terminal (Processors: " + 
							runtime.availableProcessors() + ")"  +
							"(JVM Max Mem: " + (runtime.maxMemory() / 1048576) + "MB)" +
							"(JVM Total Mem: " + (runtime.totalMemory() / 1048576) + "MB)");
		printStream.println("cls");
		printStream.println("type .\\debug_log.txt");
		printStream.println("timeout /t 1 > nul");
		printStream.println("goto start");
		printStream.close();
	}
	
	public Debugger(DebugTextColor debugTextColor) throws IOException {
		fileBat = new File("debugger.bat");
		runtime = Runtime.getRuntime();
		
		printStream = new PrintStream(fileBat);
		printStream.println("@echo off");
		printStream.println(":start");
		printStream.println("Color " + debugTextColor.color());
		printStream.println("MODE 120,30");
		printStream.println("TITLE Debug Terminal (Processors: " + 
							runtime.availableProcessors() + ")" +
							"(JVM Max Mem: " + (runtime.maxMemory() / 1048576) + "MB)" +
							"(JVM Total Mem: " + (runtime.totalMemory() / 1048576) + "MB)");
		printStream.println("cls");
		printStream.println("type .\\debug_log.txt");
		printStream.println("timeout /t 1 > nul");
		printStream.println("goto start");
		printStream.close();
		
	}
	
	public void init(boolean isAppendable) throws IOException {
		printStream = new PrintStream(new FileOutputStream("debug_log.txt", isAppendable));
		process     = Runtime.getRuntime().exec("cmd /c start cmd.exe /K .\\debugger.bat");
	}

	public void init() throws IOException {
		printStream = new PrintStream(new FileOutputStream("debug_log.txt", false));
		process     = Runtime.getRuntime().exec("cmd /c start cmd.exe /K .\\debugger.bat");
	}	
	
	public void printdbg(boolean boolData) {
		printStream.println(boolData);
	}
	
	public void printdbg(byte byteData) {
		printStream.println(byteData);
	}

	public void printdbg(char charData) {
		printStream.println(charData);
	}

	public void printdbg(short shortData) {
		printStream.println(shortData);
	}

	public void printdbg(int intData) {
		printStream.println(intData);
	}	
	
	public void printdbg(long longData) {
		printStream.println(longData);
	}
	
	public void printdbg(float floatData) {
		printStream.println(floatData);
	}
	
	public void printdbg(double doubleData) {
		printStream.println(doubleData);
	}
	
	public void printdbg(Object objectData)  {
		printStream.println(objectData);
	}
	
	public void printdbg(String msg, boolean boolData) {
		printStream.println(msg + boolData);
	}
	
	public void printdbg(String msg, byte byteData) {
		printStream.println(msg + byteData);
	}

	public void printdbg(String msg, char charData) {
		printStream.println(msg + charData);
	}

	public void printdbg(String msg, short shortData) {
		printStream.println(msg + shortData);
	}

	public void printdbg(String msg, int intData) {
		printStream.println(msg + intData);
	}	
	
	public void printdbg(String msg, long longData) {
		printStream.println(longData);
	}
	
	public void printdbg(String msg, float floatData) {
		printStream.println(msg + floatData);
	}
	
	public void printdbg(String msg, double doubleData) {
		printStream.println(msg + doubleData);
	}
	
	public void printdbg(String msg, Object objectData) {
		printStream.println(msg + objectData);
	}
	
	public static<T, U> U testBlock(T vl, Function<T, U> func) {
		if(ON) {
			Optional<T> retorno;
			
			System.out.println("(Debug) " + 
							   "Class: " + Thread.currentThread().getStackTrace()[2].getClassName() + "\\" +
							   "Method: " + Thread.currentThread().getStackTrace()[2].getMethodName() +
							   " - Line: " + Thread.currentThread().getStackTrace()[2].getLineNumber());
			
			retorno = Optional.ofNullable(vl);
			
			long startTime = System.currentTimeMillis();
			
			if(retorno.isPresent()) {
				System.out.println("(Debug) Variable type: " + vl.getClass().getName());
				
				try {
					return func.apply(vl);
				} finally {
					System.out.println();
					System.out.println("(Debug) Execution time: " + (System.currentTimeMillis() - startTime) / 1000d + "s");
					System.out.println("(Debug) Complete");
				}
			} else {
				System.out.println();
				System.out.println("(Erro) Null variable. To avoid exception, this block was terminated");
				System.out.println();
				System.out.println("(Debug) Execution time: " + (System.currentTimeMillis() - startTime) / 1000d + "s");
				System.out.println("(Debug) Complete");
			}

		}
		
		
		return null;
	}
	
	public static<T, U> U testBlock(T vl, Function<T, U> func, String msg) {
		if(ON) {
			Optional<T> retorno;
			
			System.out.println("(" + msg + ") " + 
							   "Class: " + Thread.currentThread().getStackTrace()[2].getClassName() + "\\" +
							   "Method: " + Thread.currentThread().getStackTrace()[2].getMethodName() +
							   " - Line: " + Thread.currentThread().getStackTrace()[2].getLineNumber());
			
			retorno = Optional.ofNullable(vl);
			
			long startTime = System.currentTimeMillis();
			
			if(retorno.isPresent()) {
				System.out.println("(" + msg + ") " + "Variable type: " + vl.getClass().getName());
				
				try {
					return func.apply(vl);
				} finally {
					System.out.println();
					System.out.println("(" + msg + ") " + "Execution time: " + (System.currentTimeMillis() - startTime) / 1000d + "s");
					System.out.println("(" + msg + ") " + "Complete");
				}
			} else {
				System.out.println();
				System.out.println("(Erro) Null variable. To avoid exception, this block was terminated");
				System.out.println();
				System.out.println("(" + msg + ") " + "Execution time: " + (System.currentTimeMillis() - startTime) / 1000d + "s");
				System.out.println("(" + msg + ") " + "Complete");
			}
		}

		return null;
	}	
	
	public static<U> U testBlock(Function<Void, U> func) {
		if(ON) {	
			System.out.println("(Debug) " + 
							   "Class: " + Thread.currentThread().getStackTrace()[2].getClassName() + "\\" +
							   "Method: " + Thread.currentThread().getStackTrace()[2].getMethodName() +
							   " - Line: " + Thread.currentThread().getStackTrace()[2].getLineNumber());
			
			long startTime = System.currentTimeMillis();
			
			System.out.println("(Debug) Variable type: No parameter");
				
			try {
				return func.apply(null);
			} finally {
				System.out.println();
				System.out.println("(Debug) Execution time: " + (System.currentTimeMillis() - startTime) / 1000d + "s");
				System.out.println("(Debug) Complete");
			}
		}
		
		return null;
		
	}
		
	// Do not use yet
	public void close() {
		process.destroy();
		fileBat.delete();
		
	}
}


