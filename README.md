# JAVA LIVE DEBUGGER
[![N|Solid](https://www.iconhot.com/icon/png/quiet/128/java-1.png)](https://www.oracle.com/technetwork/pt/java/javase/downloads/index.html)  
[![N|Solid](https://camo.githubusercontent.com/83b0e95b38892b49184e07ad572c94c8038323fb/68747470733a2f2f7777772e6865726f6b7563646e2e636f6d2f6465706c6f792f627574746f6e2e737667)](https://www.heroku.com/)  

**Developed by: Thiago dos Santos Martins**  
**Email: thiago.martins1989@gmail.com | thiago.martins@tutanota.com**  

Java library for separate window debug. Using it
you can simply redirect specific parts of source code to an
independent terminal to check for data and errors

### CHANGE LOG(Version 0.5 | 02/11/2020) 
  - Added parameterless testBlock
  
### IN DEVELOPMENT
 - Fix the terminal control. At current stage, the terminal is not exiting correctly
   when destroy is called   
 - Define try catches for possible errors
 - Linux support

### EXAMPLES  
- Block test
```java
		Debugger.testBlock("Testing 1 2 3", (String variable) -> {
			System.out.println(variable);
			return null; // allways return null if you don't care about returning data
		});
```  
- Terminal debugger
```java
    Debugger debugger = new Debugger();
    debugger.init();
    debugger.printdbg("Testing 1 2 3");
```






 
